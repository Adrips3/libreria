import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any[]): any {
    const resultadoLibros = [];

    for(const libro of value){
      if( libro.nombre.indexOf(arg) > -1){
        resultadoLibros.push(libro)
      };
    };

    return resultadoLibros;

  }

}
