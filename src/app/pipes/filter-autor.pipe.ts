import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterAutor'
})
export class FilterAutorPipe implements PipeTransform {

  transform(value: any, arg: any[]): any {
    const resultadoAutores = [];

    for(const autor of value){
      if( autor.nombre.indexOf(arg) > -1){
        resultadoAutores.push(autor)
      };
    };

    return resultadoAutores;
  }

}
