export class LibroModel{
  id:string;
  nombre:string;
  autor:string;
  editorial:string;
  idioma:string;
  descripcion:string;
  edicion:string;
  imagen:string;
  vivo:boolean;
  constructor(){
    this.vivo=true;
  }
}
