import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AutorModel } from '../models/autor.model';
import { map, delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private url ='https://libreria-2c9e4.firebaseio.com'

  constructor( private http:HttpClient) { }

  crearAutor( autor : AutorModel ){
    return this.http.post(`${this.url}/autores.json`,autor)
      .pipe(
        map( (resp:any) =>{
          autor.id=resp.name;
          return autor;
        })
      )
  }

  actualizarAutor(autor:AutorModel){
    const autorTemp={
      ...autor
    };

    delete autorTemp.id;

    return this.http.put(`${this.url}/autores/${autor.id}.json`, autorTemp);
  }


  getAutores(){
    return this.http.get(`${ this.url }/autores.json`)
    .pipe(
      map( this.crearArreglo ),
      delay(1500)
    )
  }
  private crearArreglo(autoresObj:object){
    const autores:AutorModel[]=[];

    if(autoresObj===null){
      return [];
    }

    Object.keys(autoresObj).forEach(key=>{
      const autor: AutorModel = autoresObj[key];
      autor.id=key;
      autores.push(autor);
    });

    return autores;
  }

  getAutor(id:string){
    return this.http.get (`${this.url}/autores/${id}.json`);
  }

  deleteAutor(id:string){
    return this.http.delete(`${this.url}/autores/${id}.json`)
  }

  buscarAutor(termino:string){
    return this.http.get (`${this.url}/autores/${termino}.json`);
  }

}
