import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LibroModel } from '../models/libro.model';
import { map, delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LibrosService {
  private url ='https://libreria-2c9e4.firebaseio.com'

  constructor( private http:HttpClient) { }

  crearLibro( libro : LibroModel ){
    return this.http.post(`${this.url}/libros.json`,libro)
      .pipe(
        map( (resp:any) =>{
          libro.id=resp.name;
          return libro;
        })
      )
  }

  actualizarLibro(libro:LibroModel){
    const libroTemp={
      ...libro
    };

    delete libroTemp.id;

    return this.http.put(`${this.url}/libros/${libro.id}.json`, libroTemp);
  }


  getLibros(){
    return this.http.get(`${ this.url }/libros.json`)
    .pipe(
      map( this.crearArreglo ),
      delay(1500)
    )
  }
  private crearArreglo(librosObj:object){
    const libros:LibroModel[]=[];

    if(librosObj===null){
      return [];
    }

    Object.keys(librosObj).forEach(key=>{
      const libro: LibroModel = librosObj[key];
      libro.id=key;
      libros.push(libro);
    });

    return libros;
  }

  getLibro(id:string){
    return this.http.get (`${this.url}/libros/${id}.json`);
  }

  deleteLibro(id:string){
    return this.http.delete(`${this.url}/libros/${id}.json`)
  }


}
