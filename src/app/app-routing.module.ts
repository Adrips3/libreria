import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './Components/index/index.component';
import { LibroComponent } from './Components/libro/libro.component';
import { LibrosComponent } from './Components/libros/libros.component';
import { AutoresComponent } from './Components/autor/autores/autores.component';
import { AutorComponent } from './Components/autor/autor/autor.component';

const routes:Routes=[
  {path:'home', component:IndexComponent},
  {path:'libros', component:LibrosComponent},
  {path:'libro/:id', component:LibroComponent},
  {path:'autores', component:AutoresComponent},
  {path:'autor/:id', component:AutorComponent},
  {path:'**', pathMatch:'full',redirectTo:'home'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ]
})

export class AppRoutingModule { }
