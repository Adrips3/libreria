import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {NgForm} from '@angular/forms';
import { Observable } from 'rxjs';

import { LibrosService } from '../../services/libros.service';
import { LibroModel } from '../../models/libro.model';

import Swal from "sweetalert2";


@Component({
  selector: 'app-libro',
  templateUrl: './libro.component.html',
  styleUrls: ['./libro.component.scss']
})
export class LibroComponent implements OnInit {
  libro = new LibroModel()
  constructor(private _libros:LibrosService,
              private route:ActivatedRoute) { };

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    if( id !== 'nuevo' ){
      this._libros.getLibro( id )
      .subscribe( (resp:LibroModel) => {
        this.libro=resp;
        this.libro.id=id;
      });
    }

  }


  guardar(form:NgForm){
    if(form.invalid){
      console.log('formulario no válido')
      return;
    }
    Swal.fire({
      title:'Espere',
      text:'Guardar información',
      type:'info',
      allowOutsideClick:false
    }as any);

    Swal.showLoading();

    let peticion:Observable<any>;

    if(this.libro.id){
      peticion = this._libros.actualizarLibro(this.libro);
    }else{
      peticion = this._libros.crearLibro(this.libro);
    }
    peticion.subscribe(resp=>{
      Swal.fire({
        title: this.libro.nombre,
        text:'Se actualizo correctamente',
        type:'success'
      }as any);
    })
  }

}
