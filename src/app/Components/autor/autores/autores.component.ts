import { Component, OnInit } from '@angular/core';
import { AutoresService } from '../../../services/autores.service';
import { AutorModel } from '../../../models/autor.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styleUrls: ['./autores.component.scss']
})
export class AutoresComponent implements OnInit {

  autores:AutorModel [] = [];
  cargando = false;
  filterAutores='';

  constructor(private _autores:AutoresService) { }

  ngOnInit() {
    this.todosLibros();
  }

  todosLibros(){
    this.cargando = true;

    this._autores.getAutores()
    .subscribe(resp=>{
      this.autores=resp;
      this.cargando=false;
    })
  }

  borrarAutor(autor:AutorModel, i:number){
    Swal.fire({
      title:'Borrar',
      text:`¿Está seguro que quiere eleminar el siguiente autor ${autor.nombre}?`,
      type:'question',
      showConfirmButton:true,
      showCancelButton:true
    }as any).then(resp=>{
      if(resp.value){
        this.autores.splice(i,1);
        this._autores.deleteAutor(autor.id).subscribe();
      }
    });
  }

}
