import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { AutoresService } from '../../../services/autores.service';
import { AutorModel } from '../../../models/autor.model';

import Swal from "sweetalert2";

@Component({
  selector: 'app-autor',
  templateUrl: './autor.component.html',
  styleUrls: ['./autor.component.scss']
})
export class AutorComponent implements OnInit {

  autor = new AutorModel();

  constructor(private _autores:AutoresService,
              private route:ActivatedRoute) { };

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    if( id !== 'nuevo' ){
      this._autores.getAutor( id )
      .subscribe( (resp:AutorModel) => {
        this.autor=resp;
        this.autor.id=id;
      });
    }

  }


  guardar(form:NgForm){
    if(form.invalid){
      console.log('formulario no válido')
      return;
    }
    Swal.fire({
      title:'Espere',
      text:'Guardar información',
      type:'info',
      allowOutsideClick:false
    }as any);

    Swal.showLoading();

    let peticion:Observable<any>;

    if(this.autor.id){
      peticion = this._autores.actualizarAutor(this.autor);
    }else{
      peticion = this._autores.crearAutor(this.autor);
    }
    peticion.subscribe(resp=>{
      Swal.fire({
        title: this.autor.nombre,
        text:'Se actualizo correctamente',
        type:'success'
      }as any);
    })
  }

}
