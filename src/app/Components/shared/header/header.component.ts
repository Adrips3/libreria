import { Component, OnInit } from '@angular/core';
import { AutoresService } from '../../../services/autores.service';
import { LibrosService } from '../../../services/libros.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private _autores:AutoresService,
              private _libros:LibrosService) { }

  ngOnInit() {
  }

  buscar(termino:string){
    console.log(termino)
    this._autores.buscarAutor(termino)
    .subscribe(data=>{
      console.log(data)
    })
  }

}
