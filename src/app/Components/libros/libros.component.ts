import { Component, OnInit } from '@angular/core';
import { LibrosService } from '../../services/libros.service';
import { LibroModel } from '../../models/libro.model';
import Swal from 'sweetalert2';

import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.scss']
})

export class LibrosComponent implements OnInit {
  libros : LibroModel [] = [];
  cargando = false;
  filterlibros='';

  constructor(private _libros:LibrosService) { }

  ngOnInit() {
    this.todosLibros();
  }


  todosLibros(){
    this.cargando = true;
    this._libros.getLibros()
    .subscribe(resp=>{
      this.libros=resp;
      this.cargando=false;
    })
  }

  borrarLibro(libro:LibroModel, i:number){
    Swal.fire({
      title:'Borrar',
      text:`¿Está seguro que quiere eleminar el siguiente libro ${libro.nombre}?`,
      type:'question',
      showConfirmButton:true,
      showCancelButton:true
    }as any).then(resp=>{
      if(resp.value){
        this.libros.splice(i,1);
        this._libros.deleteLibro(libro.id).subscribe();
      }
    });
  }

}
