import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/shared/header/header.component';
import { FooterComponent } from './Components/shared/footer/footer.component';
import { IndexComponent } from './Components/index/index.component';
import { LibroComponent } from './Components/libro/libro.component';
import { LibrosComponent } from './Components/libros/libros.component';
import { AutoresComponent } from './Components/autor/autores/autores.component';
import { AutorComponent } from './Components/autor/autor/autor.component';

import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule, MatInputModule} from '@angular/material';
import { FilterPipe } from './pipes/filter.pipe';
import { FilterAutorPipe } from './pipes/filter-autor.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    IndexComponent,
    LibroComponent,
    LibrosComponent,
    AutoresComponent,
    AutorComponent,
    FilterPipe,
    FilterAutorPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SweetAlert2Module,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    NoopAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
